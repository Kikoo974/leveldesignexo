﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timer;
    public bool stop =false;
    int temp;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(stop==false)
        {
            timer.text = "" + (int)Time.time;
            temp = (int)Time.time;
        }
        
    }
    public void StopTimer()
    {
        stop = true;
        if (temp<30)
        {
            timer.text = "Votre temps : " + temp + "  Bien joué vous avez la médaille d'or";
        }
        else if(30<=temp && temp<60)
        {
            timer.text = "Votre temps : " + temp + "  Bien joué vous avez la médaille d'argent";
        }
        else if(60<=temp && temp<100)
        {
            timer.text = "Votre temps : " + temp + "  Bien joué vous avez la médaille de bronze";
        }
        else
        {
            timer.text = "Votre temps : " + temp + "  Vous etes trop mauvais pour ce jeu";
        }

    }
}
