﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tp : MonoBehaviour
{
    public Transform pos;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {

            other.transform.position = new Vector3(pos.position.x, pos.position.y, pos.position.z);
            StartCoroutine(TPON());

        }
    }
 IEnumerator TPON()
    {
        yield return new WaitForSeconds(0.5f);
        GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(pos.position.x, pos.position.y, pos.position.z);
    }
}
  
