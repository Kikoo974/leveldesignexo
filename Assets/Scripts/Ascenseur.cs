﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ascenseur : MonoBehaviour
{
    public GameObject plat;
    bool asc = false;
    Vector3 start;
    // Start is called before the first frame update
    void Start()
    {
        start = plat.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (asc)
        {
            float i =  Time.deltaTime;
            if(i<5)
            {
                plat.transform.position = new Vector3(plat.transform.position.x, plat.transform.position.y + Time.deltaTime, plat.transform.position.z);
            }
        }
        if(plat.transform.position.y >3.65)
        {
            asc = false;
            plat.transform.position = start;
        }

    }

    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag =="Player" && Input.GetKeyDown(KeyCode.E))
        {
            asc = true;
        }
    }
    
}
