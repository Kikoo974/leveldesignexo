﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    public float second;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("test");
            StartCoroutine(ObjectDestroy());
        }
    }
    IEnumerator ObjectDestroy()
    {
        Debug.Log("test");
        yield return new WaitForSeconds(second);
        Destroy(this.gameObject);
    }
}
